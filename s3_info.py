import boto3
import boto3.session
import argparse
import datetime
import yaml
from dateutil.tz import tzutc
import re
import sys
import signal
import time

def signal_handler(signal, frame):
        print('You pressed Ctrl+C! exiting')
        sys.exit(0)

def new_s3_client():
    session = boto3.session.Session()
    return session.client('s3')
    
def bucket_slow_calculation(bucket, **kwargs):
    if storageType:
        bucket_byte_size={}
        bucket_count={}
    else:
        bucket_byte_size=0
        bucket_count=0
    last_updated_date=None

    prefix=''
    if 'prefix' in kwargs:
        prefix = kwargs['prefix']

    paginator = s3.get_paginator('list_objects_v2')

    bucket_info = paginator.paginate(Bucket=bucket, Prefix=prefix)

    for page in bucket_info:
        if 'Contents' in page:
            for key in page['Contents']:
                if storageType:
                    try:
                        bucket_count[key['StorageClass']] += 1
                    except KeyError:
                        bucket_count[key['StorageClass']] = 1
                    try:
                        bucket_byte_size[key['StorageClass']] += key['Size']
                    except KeyError:
                        bucket_byte_size[key['StorageClass']] = key['Size']
                else:
                    bucket_count += 1
                    bucket_byte_size += key['Size']

                if last_updated_date == None or key['LastModified'] > last_updated_date:
                    last_updated_date = key['LastModified']
    return bucket_count, bucket_byte_size, last_updated_date


def print_output(output):
    print("==RESULTS==\n")
    print(yaml.dump(output, default_flow_style=False))

def convert_size(size,units):
    convert={'B': {'exp':1024 ** 0,'units': 'bytes'},
             'K': {'exp':1024 ** 1,'units': 'KB'},
             'M': {'exp':1024 ** 2,'units': 'MB'},
             'G': {'exp':1024 ** 3,'units': 'GB'},
             'T': {'exp':1024 ** 4,'units': 'TB'}
            }
    if units != 'B':
        return '{0:.4f}'.format(size / convert[units]['exp']) + " " + convert[units]['units']
    else:
        return '{0:.0f}'.format(size / convert[units]['exp']) + " " + convert[units]['units']

def get_bucket_region(session,bucket):
    resp = session.get_bucket_location(Bucket=bucket)
    if resp['LocationConstraint'] == None:
        return 'us-east-1'
    else:
        return resp['LocationConstraint']

def sort_by_region(data,reverse=False):
    sortedByRegion = sorted(data, key=lambda k: k['Region'], reverse=reverse)
    return sortedByRegion

def get_cloudwatch_size(bucket,region):
    now = datetime.datetime.now()
    cw = boto3.client('cloudwatch', region_name=region)
    response = cw.get_metric_statistics(Namespace='AWS/S3',
                                        MetricName='BucketSizeBytes',
                                        Dimensions=[
                                            {'Name': 'BucketName', 'Value': bucket},
                                            {'Name': 'StorageType', 'Value': 'StandardStorage'}
                                        ],
                                        Statistics=['Average'],
                                        Period=3600,
                                        StartTime=(now - datetime.timedelta(days=1)).isoformat(),
                                        EndTime=now.isoformat())
    if len(response["Datapoints"]) != 0:
        return response["Datapoints"][0]['Average']
    else:
        return 0

def get_cloudwatch_count(bucket,region):
    now = datetime.datetime.now()
    cw = boto3.client('cloudwatch', region_name=region)
    response = cw.get_metric_statistics(Namespace='AWS/S3',
                                                MetricName='NumberOfObjects',
                                                Dimensions=[
                                                    {'Name': 'BucketName', 'Value': bucket},
                                                    {'Name': 'StorageType', 'Value': 'AllStorageTypes'}
                                                ],
                                                Statistics=['Average'],
                                                Period=3600,
                                                StartTime=(now - datetime.timedelta(days=1)).isoformat(),
                                                EndTime=now.isoformat())

    if len(response["Datapoints"]) != 0:
        return response["Datapoints"][0]['Average']
    else:
        return 0

def filter_buckets(buckets,named_buckets,regex):
    filtered_buckets=[]

    if regex:
        pattern = re.compile(regex)

    for bucket in buckets:
        if named_buckets:
            if bucket['Name'] in named_buckets:
                filtered_buckets.append(bucket)
                continue
        if regex:
            if re.search(pattern,bucket['Name']):
                filtered_buckets.append(bucket)

    return filtered_buckets

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="S3 Analyser")
    parser.add_argument("--units", dest='units', default='B', choices=['B','K','M','G','T'], action='store', help="show space in units ([B]ytes|[K]B|[M]B|[G]B|[T]B)")
    parser.add_argument("--byregion", dest='byRegion', default=False, action='store_true', help="Sort buckets by regions")
    parser.add_argument("--buckets", dest='buckets', nargs='+', default=None, help='list of buckets to check')
    parser.add_argument("--rebucket", dest='rebucket', action='store', default=None, help="bucket regex filter ie: '^test' 'blabla' 'something$' ")
    parser.add_argument("--storagetype", dest='storageType', default=False, action='store_true', help="Get size and count info per storage type")
    parser.add_argument("--prefix", dest='prefix', default=None, action='store', help="stats only for this prefix ie: folder2/subfolder1")
    parser.add_argument("--justDoIt", dest='justDoIt', default=False, action='store_true', help="skip message that it might be long....")
    parser.add_argument("--doitfast", dest='doItFast', default=False, action='store_true', help="Get metrics with cloudwatch, will remove last updated file info")
    args = parser.parse_args()

    units = args.units
    doItFast = args.doItFast
    sortByRegion = args.byRegion
    checkAllBuckets = True
    storageType= args.storageType
    buckets_to_check=args.buckets
    reBucket= args.rebucket
    prefix= args.prefix
    justDoIt = args.justDoIt


    if buckets_to_check or reBucket:
        checkAllBuckets = False

    if not justDoIt and (not doItFast and (buckets_to_check == None and reBucket == None and prefix == None)):
        print("No filtering is being applied and option doitfast not selected. This could be long if you have many many many files...")
        signal.signal(signal.SIGINT, signal_handler)
        print('you can ctrl+c to quit now. will continue in 5s ( you can skip this message with option --justDoIt )')
        time.sleep(5)

    s3 = new_s3_client()
    output=[]
    buckets = s3.list_buckets()['Buckets']
    # filter bucket list
    if not checkAllBuckets:
        buckets=filter_buckets(buckets,buckets_to_check,reBucket)

    print("Crunching data....")
    if doItFast:
        print("FAST MODE aka CLOUDWATCH MODE aka ESTIMATED MODE")
        for bucket in buckets:
            bucket_size={}
            bucket_file_count={}
            bucket_region = get_bucket_region(s3,bucket['Name'])
            bucket_size['TOTAL EST.'] = convert_size(get_cloudwatch_size(bucket['Name'],bucket_region),units)
            bucket_file_count = int(get_cloudwatch_count(bucket['Name'],bucket_region))
            output.append({'Bucket Name': bucket['Name'],
                     'Creation Date': str(bucket['CreationDate']),
                     'File Count': bucket_file_count,
                     'Size': bucket_size,
                     'Region': bucket_region})
    else:
        for bucket in buckets:
            # calculate size and number of files
            bucket_size={}
            bucket_total_byte_size=0
            if prefix:
                bucket_file_count,bucket_byte_size, last_updated_date = bucket_slow_calculation(bucket['Name'],prefix=prefix)
            else:
                bucket_file_count,bucket_byte_size, last_updated_date = bucket_slow_calculation(bucket['Name'])
    
            # calculate total size and storage type totals and add units
            if storageType:
                bucket_total_byte_size= sum(bucket_byte_size.values())
                bucket_size['TOTAL'] = convert_size(bucket_total_byte_size,units)
                for key in bucket_byte_size:
                    bucket_size[key] = convert_size(bucket_byte_size[key],units)
        
                if bucket_file_count == None:
                    bucket_file_count = 0
                else:
                    bucket_file_count['TOTAL'] = sum(bucket_file_count.values())
            else:
                bucket_size=convert_size(bucket_byte_size,units)
    
            bucket_region = get_bucket_region(s3,bucket['Name'])
            output.append({'Bucket Name': bucket['Name'],
                      'Creation Date': str(bucket['CreationDate']),
                      'File Count': bucket_file_count,
                      'Size': bucket_size,
                      'Last Updated': str(last_updated_date),
                      'Region': bucket_region})

    # apply sorting
    if sortByRegion:
        output = sort_by_region(output)
    if output:
        print_output(output)
    else:
        print("Nothing to show you.")