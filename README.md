### Pre-requis

`python` 
`virtualenv`

### Setup

Ce script assume que les crédentials sont dans (``~/.aws/credentials``)

    [default]
    aws_access_key_id = KEY
    aws_secret_access_key = SECRET

 ### Installation

    $ virtualenv -p /usr/bin/python3 s3_info
    $ source s3_info/bin/activate
    $ pip install -r requirements.txt

### Utilisation
```
usage: s3_info.py [-h] [--units {B,K,M,G,T}] [--byregion]
                  [--buckets BUCKETS [BUCKETS ...]] [--rebucket REBUCKET]
                  [--storagetype] [--prefix PREFIX] [--justDoIt] [--doitfast]

S3 Analyser

optional arguments:
  -h, --help            show this help message and exit
  --units {B,K,M,G,T}   show space in units ([B]ytes|[K]B|[M]B|[G]B|[T]B)
  --byregion            Sort buckets by regions
  --buckets BUCKETS [BUCKETS ...]
                        list of buckets to check
  --rebucket REBUCKET   bucket regex filter ie: '^test' 'blabla' 'something$'
  --storagetype         Get size and count info per storage type
  --prefix PREFIX       stats only for this prefix ie: folder2/subfolder
  --justDoIt            skip message that it might be long....
  --doitfast            Get metrics with cloudwatch, will remove last updated
                        file info
```

```
s3_info.py
No filtering is being applied and option doitfast not selected. This could be long if you have many many many files...
you can ctrl+c to quit now. will continue in 5s ( you can skip this message with --justDoIt )
^CYou pressed Ctrl+C! exiting
```
s3_info.py  --doitfast

Crunching data....
FAST MODE aka CLOUDWATCH MODE aka ESTIMATED MODE
==RESULTS==

- Bucket Name: test01-default-region
  Creation Date: '2018-02-11 18:15:52+00:00'
  File Count: 28687
  Region: us-east-1
  Size:
    TOTAL EST.: 2102982 bytes
- Bucket Name: test02-plarivee
  Creation Date: '2018-02-11 23:14:43+00:00'
  File Count: 0
  Region: ca-central-1
  Size:
    TOTAL EST.: 0 bytes
- Bucket Name: test03-plarivee
  Creation Date: '2018-02-11 23:15:05+00:00'
  File Count: 0
  Region: eu-west-3
  Size:
    TOTAL EST.: 0 bytes
- Bucket Name: test04-plarivee
  Creation Date: '2018-02-12 16:25:20+00:00'
  File Count: 0
  Region: ca-central-1
  Size:
    TOTAL EST.: 0 bytes
```

```
 s3_info.py  --unit M --buckets test04-plarivee
Crunching data....
==RESULTS==

- Bucket Name: test04-plarivee
  Creation Date: '2018-02-12 16:25:20+00:00'
  File Count: 0
  Last Updated: None
  Region: ca-central-1
  Size: 0.0000 MB
```

```
s3_info.py  --unit M --byregion --storagetype
Crunching data....
==RESULTS==

- Bucket Name: test02-plarivee
  Creation Date: '2018-02-11 23:14:43+00:00'
  File Count:
    STANDARD: 13005
    TOTAL: 13005
  Last Updated: '2018-02-12 14:20:54+00:00'
  Region: ca-central-1
  Size:
    STANDARD: 0.3239 MB
    TOTAL: 0.3239 MB
- Bucket Name: test04-plarivee
  Creation Date: '2018-02-12 16:25:20+00:00'
  File Count:
    TOTAL: 0
  Last Updated: None
  Region: ca-central-1
  Size:
    TOTAL: 0.0000 MB
- Bucket Name: test03-plarivee
  Creation Date: '2018-02-11 23:15:05+00:00'
  File Count:
    TOTAL: 0
  Last Updated: None
  Region: eu-west-3
  Size:
    TOTAL: 0.0000 MB
- Bucket Name: test01-default-region
  Creation Date: '2018-02-11 18:15:52+00:00'
  File Count:
    REDUCED_REDUNDANCY: 2
    STANDARD: 28685
    TOTAL: 28687
  Last Updated: '2018-02-12 17:29:32+00:00'
  Region: us-east-1
  Size:
    REDUCED_REDUNDANCY: 0.0015 MB
    STANDARD: 0.7146 MB
    TOTAL: 0.7160 MB
```

```
s3_info.py --rebucket 'region$' --buckets test02-plarivee
Crunching data....
==RESULTS==

- Bucket Name: test01-default-region
  Creation Date: '2018-02-11 18:15:52+00:00'
  File Count: 28687
  Last Updated: '2018-02-12 17:29:32+00:00'
  Region: us-east-1
  Size: 750813 bytes
- Bucket Name: test02-plarivee
  Creation Date: '2018-02-11 23:14:43+00:00'
  File Count: 13005
  Last Updated: '2018-02-12 14:20:54+00:00'
  Region: ca-central-1
  Size: 339660 bytes
```

```
s3_info.py --buckets test01-default-region --prefix folder2/subfolder1 --rebucket test02
Crunching data....
==RESULTS==

- Bucket Name: test01-default-region
  Creation Date: '2018-02-11 18:15:52+00:00'
  File Count: 1300
  Last Updated: '2018-02-11 23:54:06+00:00'
  Region: us-east-1
  Size: 33966 bytes
- Bucket Name: test02-plarivee
  Creation Date: '2018-02-11 23:14:43+00:00'
  File Count: 1300
  Last Updated: '2018-02-12 14:07:50+00:00'
  Region: ca-central-1
  Size: 33966 bytes

```
