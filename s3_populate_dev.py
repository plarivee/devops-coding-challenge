import boto3

aws_s3_buckets = ['test02-plarivee']
s3 = boto3.client('s3')

for bucket in aws_s3_buckets:
	for folder in range(1,6):
		folder_key ="folder" + str(folder) + "/"
		s3.put_object(Bucket=bucket, Body='', Key=folder_key)
		for subfolder in range(1,3):
			subfolder_key=folder_key + "subfolder" + str(subfolder) + "/"
			s3.put_object(Bucket=bucket, Body='', Key=subfolder_key)
			for file in range(1,1300):
				file_key = subfolder_key + "file" + str(file)
				s3.put_object(Bucket=bucket, Body=file_key, Key=file_key)
